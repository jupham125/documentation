### Release branch manager name

John R. Doe

### Intended support window 

Feb 2020 - Sep 2020

### Contact information

* doej@redfield.dev
* @doej on Freenode

### Release Candidate (RC) schedule

* RC0 (Nov 1 2019) - Initial build of branch and verification of CI components (don't include feature foobar)
* RC1 (Nov 15 2019) - Incorporate fixes from RC0 and add feature foobar
* RC2 (Dec 1 2019) - Incorporate fixes from bug reports on RC1 and feature fixes for foobar
* Final Release (Dec 15 2019) - Incorporate minor fixes into RC2, create tagged release.

### Some details on your intended targets for core components (Xen version, Linux kernel version, Yocto release version, etc)

* Xen v9.5
* Linux v12.3
* Yocto v4.2
* Golang 2.2 

### Your intended level of support (are you going to backport CVEs, XSAs, etc., package up-revving)

* Backports will be derived from their upstream projects (Xen, Linux, Yocto)

### The CI resources you will provide to support the release

* A server will be provisioned to mirror the meta-redfield CI builder for the assigned branch

