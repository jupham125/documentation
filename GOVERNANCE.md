# Terminology

* A Contributor is any individual creating or commenting on an issue or pull request.

* A Maintainer is a contributor that is responsible for a specific area.  Maintainers are responsible for ensuring quality of contributions for their area of responsibility.

* A Committer is a subset of contributors who have been given write access to the repository.  Under this project, maintainers are committers.

* A TC (Technical Committee) is a group of committers representing the required technical expertise to resolve rare disputes.

# Governance Model

This project operates under a liberal contribution model.  Under a liberal contribution model, the people who do the most work are recognized as most influential, but this is based on current work and not historic contributions. Major project decisions are made based on a consensus seeking process (discuss major grievances) rather than pure vote, and strive to include as many community perspectives as possible.  For irreconcilable objections, as a matter of last resort, the TC will decide how to proceed and inform the community.

# Current Maintainers/Committers

You may see the current list of committers by looking at the member list on the gitlab site:
* https://gitlab.com/groups/redfield/-/group_members

# Current Technical Committee Members

* Brendan Kerrigan

* Chris Patterson

* Eric Chanudet

* Jed Lejosne

# Technical Committee Changes

Any project maintainer may nominate a new member into the TC.  This can be done with a merge request to this repository, changing this document.

Existing TC members may volunteer to remove themselves through a merge request as well.  This is strongly encouraged for inactive TC members.

# Redfield Release Process

## Introduction
The Redfield distribution generally follows the bleeding-edge of the upstream components it's built on, but there are scenarios where a stable release branch is desirable (i.e. when a downstream project wants to do a release itself). Any community member may request a stable branch for creating a release as long as they provide some information on their commitment to maintenance and management of the release. While branching is on paper a low-cost endeavor for the project, there are non-zero costs in resources associated with maintaining a release and its associated branch.

## Requesting a Release Branch
To request a release branch: 
1) Create an issue asking for a release branch in the [documentation repository](https://gitlab.com/redfield/documentation/issues).
2) Title the request *Release branch request*
3) Provide the following information within the issue:
    * Release branch manager name
    * Intended support window (how long do you intend to support the release)
    * Contact information (provide at least a well tended to email address, but preferably also your IRC handle on freenode)
    * Your intended release candidate (RC) schedule, including a final release date
    * Some details on your intended targets for core components (Xen version, Linux kernel version, Yocto release version, etc)
    * Your intended level of support (are you going to backport CVEs, XSAs, etc., package up-revving)
    * The CI resources you will provide to support the release

The decision to create a release branch will be discussed on the issue ticket itself, and currently there isn't any clear criteria for acceptance or rejection (though this will be more thoroughly defined as we go through the process). Just some example things that might warrant a rejection (but are up for discussion on the request issue itself):
 * No CI Resources (the release branch should have an OE builder and host build products at a minimum)
 * Is not significantly differentiated from an available release branch (let's share the maintenance burden!)
 * Very small supported window (supporting a release for 2 months probably doesn't warrant a stable branch)
 * No or too few release candidates scheduled

To be clear, this is a fluid process and open to refinement. When a release branch request is approved, that release branch will be given a name and created. Permissions will be set up so the release manager has authority to both merge and push to the release branch.

## Creating a Release Branch
Maintainers can create a release branch with the following command, from within their redfield work directory:
```
$ BRANCH=somename make branch-create
```
Once that is completed, the branch must be set to "protected" via the Gitlab UI. And, if the branch maintainer is not a Redfield project maintainer, they must be given merge rights on the branch.

## Creating a Release Candidate (Tag)
Release branch maintainers can create a release candidate tag with the following command, from within their redfield work directory:
```
$ BRANCH=somename RC_NUMBER=0 make branch-tag-rc
```
An empty commit will automatically pushed to the tag that says "Tag release candidate: somename-rc0."

## Creating a Release (Tag)
Release branch maintainers can create a release tag with the following command, from within their redfield work directory:
```
$ BRANCH=somename MAJOR=0 MINOR=1 make branch-tag-release
```
An empty commit will automatically pushed to the tag that says "Tag release: somename-v0.1." Note that the major version number may be prescribed to the release branch maintainer has their request for a release branch fulfilled.

